# Master-Thesis

Initially cloned from this [repo](https://gitlab.com/UnBVision/template-cic-ingles).

## TODOS

### Sugestões Da banca
#### Krystian
- [ ] **Mudar o título**
- [ ] Medir a qualidade dos pseudo-labels (entender quantos IDs tem, quantos clusters são formados, outliers..)
- [X] Utilizar Camera batch normalization no método supervisionado
- [X] Discutir melhor o motivo do método não supervisionado superar os resultados do método supervisionado
- [X] Experimentar um novo upper bound utilizando ambos os datasets source
- [ ] * Analisar a influência de diferentes Inicializações 

#### Bruno
- [X] Foco em mais ablation studies
- [ ] *Olhar maneiras de utilizar active learning para entender o quanto cada nova sample auxilia no método


### Essencial
- [X] Fazer um resumo extendido em portugês de pelo menos 4 páginas
- [X] Colocar o texto todo num overleaf da vida para verificar erros de digitação
- [X] Arrumar a parte que diz o membros da banca, colocar a descrição embaixo do nome 
- [X] **Mexer nas tabelas do cap 4 (uma ta source -> target e a outra target -> source)**
- [X] **Arrumar tabela 5.2**
- [X] Revisar o cap 5
- [X] Inserir as novidades do artigo aceito pelo IJPRAI no cap. 4 
- [X] **Revisar o cap 4**
- [X] **Verificar se ainda faz sentido a explicação do progressive learning no cap 5 depois da inclusão desse no cap 4.**
- [X] Inserir na introdução as referências aos artigos aceitos
- [X] Checar a seção 2.4 quanto a acronimos, simbolos e notação..
- [ ] Olhar o cap 6 (Adicionar future works)

### Notação
- [X] Definir melhor os parâmetros e ser consistente a eles
- [X] Criar uma tabela de simbolos
- [X] Notação na explicação do conceito de batch hard. Nao usar Neg_i.. usar X_n1...
- [X] **Melhorar imagem do batch hard com nova notação e mais detalhes. Alterar tambem a legenda**
- [X] Explicar o m da margin da triplet loss no cap 2 e reutilizar aqui no cap 4.2.2
- [X] Reutilizar explicação do m no cap 4.2.2
- [X] Melhorar o nome das variaveis e explicar elas, tipo n_instances, num_epochs..
- [X] Melhorar a variável usada no min_s do DBSCAN seção 5.2.3
- [X] Criar uma lista de siglas
- [X] Verificar se todas as tabelas e figuras estão com a legenda em cima/baixo (padronizar)

### Melhorias no capitulo 2
- [X] Criar uma seção antes da 2.1 para introduzir melhor o problema. Se espelhar na 3.2.1
- [X] Usar boa parte da seção 3.2.1 na futura seção 2.1 que irá intorduzir várias coisas
- [X] Verificar a possibilidade de mover o inicio do cap 4 para a futura seção 2.1 (**Decidi não mover**)
- [X] **Definir redes neurais de uma maneira básica nessa nova seção 2.1 (ainda falta falar das CNNs e revisar o Perceptron e o MLP)**
- [X] Definir GAN, é falado na seçao 2.3.2
- [X] **Definir GAN, mostrar a equação de custo dela (usar outra letra para o discriminador) - citar essa equação no fim da seção 3.2.1**
- [X] Citar a seção de definição da GAN na seção 3.2.1 e arrumar o texto
- [X] Na nova seção 2.1 definir os conceitos de domain, task
- [X] Definir metric learning e pq o person Reid é um problema de metric learning na seção 2.1 (incluir citações)
- [X] Explicar o conceito de margem na constrastive loss (pois vem antes da triplet loss no texto)
<!-- - [ ] Ler a nova survey da Gabriela Csurka para melhorar a seção de transfer learning (veririficar se ainda faz sentido) -->
<!-- - [ ] Revisar a seção 2.3.1, ela chama "domain distante", mas não fala de domain disimilarity (veririficar se ainda faz sentido) -->
<!-- - [ ] Revisar a seção 2.3.1, ela fala mais de tasks do que de domains. Verificar a necessiade da figura 2.7 (veririficar se ainda faz sentido) -->
- [X] Definir unsupervised person Re-ID 
- [X] Definir unupervised domain adaptation (transductive learning as Pan & Yang would say) no person Re-ID
<!-- - [ ] Definir negative transfer, termo usado na seçao 2.3.2 (veririficar se ainda faz sentido) -->
<!-- - [ ] Melhorar o final remarks do cap 2, falar o que aprendi com a discussão de transfer learni. Falar sobre a melhor arquitetura, falar sobre as losses.... (ver anotações do Teo) (veririficar se ainda faz sentido) -->
- [X] Colocar a seção de data augmentation no cap 2, faz mais sentido
- [X] **Revisar a parte das equações na seção 2.6.1, referenciar a seção 2.4.1**
- [X] **Colocar um pequeno paragrafo de introdução na seção 2.6**
- [X] Falar do data augmentation usado na seção 5.2.1, na seção de data augmentation a ser criada no cap 2 (veririficar se ainda faz sentido)
- [X] Criar uma seção no cap 2 explicando as métricas utilizadas para validação (CMC e mAP) e as métricas utilizadas para analisar a qualidade dos clusters

### Ilustrações
- [X] **Ajeitar o gráfico de gantt**
- [X] Adicionar um diagrama explicando melhor o AlignedReid++
- [ ] Mostrar mais exemplos do dataset Viper
- [ ] Mostrar mais exemplos do dataset CUHK03
- [ ] Mostrar mais exemplos do dataset Market1501
- [ ] Mostrar mais exemplos do dataset DukeMTMC
- [ ] **Fazer um diagrama para explicar o método do cap 4, provavelmente inserir ele por volta da seção 4.2.3**
- [X] Gerar mais exemplos para a figura 4.1
- [X] Gerar mais exemplos para a figura 4.2
- [ ] Gerar exemplos negativos com problema de morfologia para a figura 4.2
- [ ] **Colocar um gráfico mostrando o scheduler da taxa de aprendizado na seção 5.2.1**

### Referências
- [X] Citar a versão do arxiv do paper do IJPRAI no publications do cap 1, secao 1.3
- [X] Citar fontes mais recentes de redes neurais no cap 2 seção Neural Networks
- [X] Citar quem fez essa normalização antes na seção 5.2.5 (pegar essa citação no teams, se não achar perguntar pro Teo)
- [X] Ajeitar a formatação das referências
<!-- - [ ] Olhar anotações do Teo e corrigir referências com problemas -->

### Trabalhos futuros (citar)
- [X] **Falar possivel trabalho futuro em treinar com os conjuntos source + target juntos (ambos anotados) para ter um novo upper bound**
- [X] **Falar possivel trabalho futuro em treinar com os conjuntos source (anotado) + target (pseudo-labels)**

### Trabalhos futuros (Realizar)
- [X] Falar possivel trabalho futuro em treinar com os conjuntos source + target juntos (ambos anotados) para ter um novo upper bound
- [X] Falar possivel trabalho futuro em treinar com os conjuntos source (anotado) + target (pseudo-labels)
- [X] Colocar uma proposta de futuros trabalhos utilizando técnicas mais recentes de taxa de aprendizado, como a oneCycleLearn da FastAI
- [X] Completar o gráfico da figura 5.2 sem repetir os valores da curva azul, mas 
fazer todos os experimentos 
<!-- - [ ] _Usar regularização MMD (maybe not)_ -->

### General

- [X] Mudar a descrição do resumo do cap 5 no outline do cap 1
- [ ] Review the copyright from every image
- [ ] Check CVF copyrights
- [ ] Explicar melhor a imagen da MLFN no cap 2 onde fala de factorization networks
- [ ] Falar mais sobre a IBN-Net
- [ ] Melhorar a explicação da IBN-Net na seção 2.1.4 e, consequentemente, melhorar a explicação do seu uso na seção 5.2.1
- [X] Trocar bibstyle para citar em [1] ao inves de superescrito
- [X] Colocar um \ após todos os pontos que não encerram frases
- [X] Inserir uma tabela resumindo os datasets (ver anotações do Teo)
- [X] Incluir a tabela que resume os datasets no fim do cap 3
- [X] **Falar da tabela no fim do cap 3**
- [X] Resumir melhor o cap 3 no final observations. uma vez que data augmentation vai cair fora. Pode ser interessante falar que pra testar bem os metodos de UDA person Re-ID o melhor par de datasets disponivel é Market + Duke pelo tamanho deles e tbm pq sao os mais usados na literatura
- [X] **Após mudar a posição da seção 3.2.1 mandando ela pro cap 2, me atentar como isso pode afetar o intermediate dataset generation na seção 4.2.3**
- [ ] Explicar o 632 na tabela 4.1, ele se distoa dos outros números
- [X] Trocar as colunas de source e target domain na tabela 4.2, seguir o padrão da 4.3
- [X] Revisar a afirmação que g é proporcional a epsilon logo abaixo da tabela 4.3
- [X] Verificar todas as referências a outras seções e inserir o \S para fazer o símbolo bonito de seção
- [ ] Explicar o grid search que foi feito para encontrar o melhor epsolon pro DBSCAN na seção 5.2.3, comentar na seção 5.2.1 da margin usada na triplet loss e usar isso para explicar a faixa de valores utilizda no DBSCAN
- [ ] Explicar na seção 5.2.5 que a normalização serve para tirar a cycleGAN do jogo e simpificar o processo.
- [X] Checar todos os lugares que eu escrevi normalization, maximize ou minimize e trocar o z pelo s (ingles UK)
- [X] Checar todos os lugares que eu escrevi center e colocar centre (ingles UK)
- [X] Checar todos os lugares que eu escrevi color e colocar colour (ingles UK)

## Duvidas
