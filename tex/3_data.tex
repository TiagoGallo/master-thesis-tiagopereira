\label{chapter:data}
As the \acrfull{ML} methods learn from the data presented, it is important 
to analyse the datasets and understand what kind of data you are using. Specially, \acrshort{CNN}s
demand a huge amount of data to obtain great results, but public datasets 
are limited. Therefore, we need to use a strong set of data augmentation techniques to produce more 
data without losing critical informations.

During the course of our work, we used four publicly available datasets for training and 
validation of our models.

Each dataset has its own characteristics and can be seen as a domain. Identifying those 
specific characteristics is essential to understand the complexity behind the person \reid 
domain adaptation challenge. Therefore, in this Chapter we analyse each one of them. 

\section{Viper}
The Viper dataset released by Gray et al.\ \cite{viper} in 2007 is the oldest dataset used in 
our work. It only contains 1264 images from 632 different persons, therefore for each person only 
one image was captured per camera. To capture these images, the authors used 2 distinct cameras, 
however they were not fixed cameras, so their position have been relocated multiple times during 
the data collection. These moving cameras generate a high variance on the images characteristics, so 
even with only 2 cameras they have challenging setup.

The authors forced some kind of variations to increase even more the diversity of images. The 
main variation applied by the authors was an angle variation, capturing images with the camera angle 
varying from 45 to 180 degrees. Also, the data collection process lasted a few days, therefore 
illumination changes are present. 

They recorded all the scenes and processed the video files to create the dataset.
All images from the dataset have a resolution of 
$48 \times 128$ pixels, although some of these images have clearly been distorted to achieve these 
dimensions. The Figure \ref{fig:viper} show images from the same person in different views from this dataset.

\begin{figure}[htbp]
    \centering

    \includegraphics[width=0.15\textwidth]{img/Viper_cam0.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/Viper_cam1.jpg}
    
    \caption{Example images from the same person in different cameras from Viper 
    dataset\cite{viper}. \copyright 2007 IEEE.}
    \label{fig:viper}
\end{figure}

\section{CUHK03}
The CUHK03 dataset released by Li et al.\ \cite{deepReID} in 2014 has 13164 images from 1360 different 
persons (an average of 4.8 images per person per camera). To create this dataset, they used up to 6 
different cameras, however each person appears in only 2 from these 6 cameras. Also, there are 2 released setups 
available for this dataset, the first relied on an object detection algorithm to annotate the person 
bounding box, while the other setup produced the person images using manual annotations. In our work, 
we choose the manually annotated setup.
%Sim, Sempre que vi outros autores citando essa diferença de setups no dataset eles diziam que usavam 
% a versao anotada manualmente. Eu tambem nunca vi o dataset anotado utilizando o detector de pedestres,
% nao sei se eh de facil acesso 

All the 6 cameras used are security cameras from \acrshort{CCTV} systems, therefore 
illumination variability and occlusions are common (this problem is minimised for the manually
annotated setup). The image resolution also has some variation because of the different 
cameras used, however the average image resolution is $100 \times 300$ pixels. In Figure 
\ref{fig:exemplo} we present an example of how the same person look in different views from CUHK03.

\begin{figure*}[htpb]%
    \centering
    
    \includegraphics[width=0.15\textwidth]{img/pessoa9_cam0.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/pessoa9_cam1.jpg}%
    
    \caption{Example images from the same person in different views from CUHK03 dataset\cite{deepReID}.
    \copyright 2014 IEEE.}%
     
    \label{fig:exemplo}%
 \end{figure*}

\section{Market1501}
The Market1501 dataset released by Zheng et al.\ \cite{zheng2015scalable} in 2015 is one of the most used 
dataset for person \reid research nowadays. This dataset is great to train \acrshort{CNN}s, because of the amount 
of available data. There are more than 32 thousand images from 1501 different people in 6 distinct camera
views, averaging 3.6 images per person per camera. Also, in this dataset they have some examples where the 
person was seen in all 6 cameras, which is great for the person \reid learning once you have examples that 
show how someone appears in all different views.

% No, the other datasets did not use actors. The majority of then were recorded in university campus
While creating this dataset, the authors listed 3 problems they wanted to solve: 
\begin{itemize}
    \item The other available datasets did not have enough data to train deep \acrshort{CNN}s;
    \item The images from other datasets were manually annotated, reducing the real world factor from the challenge;
    \item There were few example images per person in other datasets. 
\end{itemize}    
To solve these problems, the authors gathered more 
than 32 thousand images from people in a real market and merged these images with around 3 thousand
distractor images (a group of images where no person is seen). All these images were acquired and annotated 
using the using the Deformable Part Model (DPM) \cite{DPM} as pedestrian detector. Finally, these images were collected in an uncontrolled open space and they 
were able to collect examples from the same person in multiple views (for some persons all views were available).

All the images from this dataset have been resized to $64 \times 128$ pixels, Figure \ref{fig:Market1501}
shows example images from the same person in different views from this dataset.

\begin{figure}[htbp]
    \centering

    \includegraphics[width=0.15\textwidth]{img/market_cam0.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/market_cam1.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/market_cam2.jpg}
    
    \hfill

    \includegraphics[width=0.15\textwidth]{img/market_cam3.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/market_cam4.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/market_cam5.jpg}

    \caption{Example images from the same person in different cameras from Market1501 dataset
    \cite{zheng2015scalable}. \copyright 2015 IEEE.}
    \label{fig:Market1501}
\end{figure}

\section{DukeMTMC}

Originally, the DukeMTMC dataset \cite{ristani2016MTMC} was created to help accelerate the progress in multi-target, multi-camera (MTMC) 
tracking systems. Ristani et al.\ recorded 85 minutes videos from 8 distinct high resolution cameras at Duke University campus.
Therefore, they had $8 \times 85$ minutes of video recorded at 1080p, 60fps with more than 2800 identities to perform multi-camera 
tracking.

Then, in 2017, Zheng et al. \cite{zheng2017unlabeled} processed the DukeMTMC dataset to create the DukeMTMC-reID dataset, which is a 
subset of the DukeMTMC specific for image-based person \reid. For that, they followed the format of Market1501 and cropped pedestrian 
images every 2s of each video, leading to a total of 36411 images from 1812 identities, where 1404 identities appear in, at least, two 
cameras and the other 408 identities only appeared in one camera (these IDs were considered distractors). The 1404 identities that appear
in more than one camera were randomly separated in training/testing groups, so the dataset have 16522 images from 702 IDs for training and 
19889 images from 702 + 408 (distractors) IDs for testing.

The identity which appear in most distinct cameras is in the training set and have the ID 0071, this man appeared 42 times in 6 different cameras.
Figure \ref{fig:DukeMTMC_reID}
show example images from him in different views from this dataset.

\begin{figure}[htbp]
    \centering

    \includegraphics[width=0.15\textwidth]{img/duke_cam1.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/duke_cam2.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/duke_cam5.jpg}
    
    \hfill

    \includegraphics[width=0.15\textwidth]{img/duke_cam6.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/duke_cam7.jpg}
    \qquad
    \includegraphics[width=0.15\textwidth]{img/duke_cam8.jpg}

    \caption{Example images from the person 0071 in 6 different cameras from DukeMTMC-reID dataset \cite{zheng2017unlabeled}.}
    \label{fig:DukeMTMC_reID}
\end{figure}


\section{Final Observations}
As our work proposes a person \reid model that is ready for the real world challenges, we need to simulate it 
in our experiments. Therefore, having multiple datasets that were captured in real scenarios is excellent for us. 
Also, the unsupervised domain adaptation techniques that we are going to propose in the next two chapters
do not rely in the data annotation which is the exactly situation that someone would face in the real world.

In Table \ref{table:datasets} we present the statistics for all used datasets\footnote{We are aware 
of other Person \reid datasets as the Person30K \cite{Bai_2021_CVPR}, MSMT17 \cite{person_transfer_GAN},
CUHK02 \cite{Li_2013_CVPR} and PRID \cite{PRID}. We have not conducted experiments in these datasets because 
PRID and CUHK02 were too small to train deep neural networks, Person30K was released after we performed
our experiments and we had technical difficults obtaining MSMT17 dataset.}. 
It is interesting to notice how 
the ammount of samples in a dataset increased over time, this is direct related with the CNNs popularity and need 
for more training data. In addition, the variety of scenes, indoor for CUHK03 and outdoor for the others, will be an 
important factor to validate our models generalisation capacity. However, the Viper dataset is really small and their cameras 
were not fixed during the process of gathering data, these problems will reflect in our models performance. 

\begin{table}[htbp]
    \centering
    \footnotesize
    \caption{An overview of the statistics from each dataset used in this work. 
            This table was inspired in Bai et al.'s\ work\ \cite{Bai_2021_CVPR} }\label{table:datasets} 
    \begin{tabularx}{\linewidth}{ X >{\hsize=.35\hsize}X >{\hsize=.35\hsize}X >{\hsize=.35\hsize}X >{\hsize=.35\hsize}X }
        \toprule[1.5pt]
         & \multicolumn{1}{c}{Viper \cite{viper}} & \multicolumn{1}{c}{CUHK03 \cite{deepReID}}  & \multicolumn{1}{c}{Market1501 \cite{zheng2015scalable}} & \multicolumn{1}{c}{DukeMTMC \cite{ristani2016MTMC}} \\
        \midrule[1.5pt]
        Release Year &  \multicolumn{1}{c}{2007} & \multicolumn{1}{c}{2014} & \multicolumn{1}{c}{2015} &  \multicolumn{1}{c}{2016} \\
        \midrule
        Samples & \multicolumn{1}{c}{1264} & \multicolumn{1}{c}{28192} & \multicolumn{1}{c}{32668} & \multicolumn{1}{c}{36411} \\
        \midrule
        Identities & \multicolumn{1}{c}{632} & \multicolumn{1}{c}{1467} & \multicolumn{1}{c}{1501} & \multicolumn{1}{c}{1812} \\
        \midrule
        Cameras & \multicolumn{1}{c}{2} & \multicolumn{1}{c}{2} & \multicolumn{1}{c}{6} & \multicolumn{1}{c}{8} \\
        \midrule
        Avg Number of Cameras \\ Passed per Identity & \multicolumn{1}{c}{2} & \multicolumn{1}{c}{2} & \multicolumn{1}{c}{4.42} & \multicolumn{1}{c}{2.67} \\
        \midrule
        Scene & \multicolumn{1}{c}{outdoor} & \multicolumn{1}{c}{indoor} & \multicolumn{1}{c}{outdoor} & \multicolumn{1}{c}{outdoor}\\
        \bottomrule
    \end{tabularx}
\end{table}
