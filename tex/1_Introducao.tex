\label{chapter:intro}
\section{Problem}
% In the world where big data reigns and there is plenty of hardware prepared to gather a huge amount of non structured data, data acquisition is no longer a problem.
% Surveillance cameras are ubiquitous and they capture huge numbers of people walking across different scenes. 
% However, extracting value from this data is challenging, specially for tasks that involve human images, such as face recognition and person re-identification.

Person re-identification (\reid) is an image retrieval task which aims at matching person images from 
different non-overlapping cameras views (Figure \ref{fig:reid_ex}). This is an essential feature
for diverse real word challenges, such as smart cities \cite{smart_cities},
intelligent video surveillance \cite{WANG20133}, suspicious action recognition
\cite{surveillance} and pedestrian retrieval \cite{SVDNet}.

\begin{figure}[htbp]
    \centering

    {\includegraphics[width=\textwidth]{img/REID.jpeg}}%
    
    \caption{Person Re-Identification is an image retrieval task. Given a query image, Person Re-ID's objective is to 
    find images from the same person in a gallery.}
    \label{fig:reid_ex}
\end{figure}

Although person \reid and face recognition are similar problems, they have a crucial difference. 
Face recognition requires images with high quality and from a frontal face view, while person \reid works with images from 
\acrshort{CCTV} (closed-circuit-television) systems that have low resolution and varied viewpoints. 

Furthermore, personal data 
protection and privacy are mainstream topics with regulations like \acrshort{GDPR} (general data protection regulation) in Europe and 
\acrshort{LGPD} (\textit{lei geral de proteção de dados}) in Brazil. As person \reid systems do not necessarily recognise a person, only re-identifies a 
previously seen person (inside a restricted time difference), they have way less friction to be used in real-world systems.

With all these popular possible applications and advantages, there is a clear demand for
robust \reid systems in the industry. 
Academic research groups have achieved 
remarkable in-domain results on popular person \reid datasets such as Market1501 
\cite{zheng2015scalable} and DukeMTMC-reID~\cite{zheng2017unlabeled}. 
Despite these advances, there is still a 
dichotomy between the success in academic results versus the 
industrial application. This is because the best academic results 
\cite{stReid,Luo_2019_CVPR_Workshops,Zhou_2020_CVPR}
are based on supervised methods that require
a huge amount of annotated data for their training. 

The use of pre-trained state-of-the-art \reid models in new scenarios usually leads to 
disappointing results because each group of cameras has distinct 
characteristics, such as illumination, resolution, noise level, orientation, pose, distance,
focal length, amount of people's motion as well as factors that influence
the appearance of people, such as ethnicity, type of location (e.g.\ leisure vs.\ work places)
and weather conditions.

Therefore, we have a scenario where a lot of non-annotated data (from \acrshort{CCTV} systems) is available and we have some 
pre-trained models that are specialised on specific domains.
Our main research question is how to leverage from the pre-trained model knowledge %acquired with public datasets to 
to perform well in data from new scenes?


\section{Objectives}

Our main objective is to propose a person \reid framework that is capable to learn good representations from 
non-annotated data. Then, we set some auxiliary goals to help us achieve our main objective and answer the 
research question. Our auxiliary goals are:

\begin{itemize}
    % \item Acquire a deep knowledge in person \reid academic research;
    \item Implement a baseline domain adaptation method to have a baseline to start from;
    \item Identify the flaws in our baseline domain adaptation method and propose techniques to undermine them;
    \item Compare our proposed methods with the state-of-the-art algorithms.
\end{itemize}



% \section{Methodology}

% In our technique, we use a public dataset as our source domain and the non structured data from the CCTV as our target domain. 
% In our source domain all the annotation and image filtering have already been done, then we use unsupervised image-image translation to create an intermediate dataset, 
% the domain-adapted (DA) dataset.
% This dataset has the labels of the source domain, but the appearance of people is similar to those in the target domain.
% Next, we proceed to the metric learning step using the DA dataset.
% As the DA dataset is similar to the target domain, we expect that the CNN trained on it will perform well in the target domain.

% In addition, we use this learned metric to annotate the target domain using a clustering algorithm. That way, we have pseudo-labels available for the target domain, 
% then we fine-tune our CNN in these pseudo-labels and learn specific characteristics of the target domain. As the training is performed with the actual target domain 
% images we expect to increase the performance, even though the adaptation process generates a noisy label space for the target domain.

\section{Publications}
While working towards our goals, we proposed some techniques that generated the following publications:

\begin{itemize}
    \item Pereira, T. and de Campos, T. Domain Adaptation for Person Re-identification on New Unlabeled Data\cite{reid_visapp_2020}
    (best student paper award winner at VISAPP 2020)
    \item Pereira, T. and de Campos, T. Domain Adaptation for Person Re-Identification with Part 
    Alignment and Progressive Pseudo-Labeling\cite{IJPRAI_reid_2021}
    \item Pereira, T. and de Campos, T. Learn by Guessing: Multi-Step Pseudo-Label Refinement for Person Re-Identification \cite{reid_visapp_2022}
\end{itemize}

\section{Outline}

The reminder of this dissertation is organised as follows:

\begin{itemize}
    \item \textbf{Chapter 2 - Background:} we present the theoretical background acquired while researching about person \reid, the 
background discussed in this chapter will be the base knowledge to the proposed methods in chapters 4 and 5.
    \item \textbf{Chapter 3 - Datasets and data augmentation:} we discuss the datasets used in our work, also we go through 
some data processing techniques that are useful for our task.
    \item \textbf{Chapter 4 - Domain adaptation on new unlabelled data:} we present the method for our first approach where we used a 
two phase domain adaptation framework with ResNet-50 and AlignedReID++ as backbones.
    \item \textbf{Chapter 5 - Multi-Step Pseudo-Label Refinement:} although promising, the results of previous 
Chapter highlight some deficiencies of several \acrfull{UDA} person Re-ID methods. In this chapter, we propose a combination 
of techniques that addresses those limitations.
    \item \textbf{Chapter 6 - Proposal:} we conclude this dissertation and present a schedule with next steps to improve our work.
\end{itemize}